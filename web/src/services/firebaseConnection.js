import firebase from "firebase/app";
import 'firebase/firestore'; //banco de dados
import 'firebase/auth';


let firebaseConfig = {
    apiKey: "AIzaSyBZ2nr0bHstqLXXIVsjbjfZtKHPOigRHGs",
    authDomain: "sistema-20037.firebaseapp.com",
    projectId: "sistema-20037",
    storageBucket: "sistema-20037.appspot.com",
    messagingSenderId: "705227677300",
    appId: "1:705227677300:web:a628a98f92d12bae42fb21",
    measurementId: "G-XPM00VWJGG"
  };

  firebase.initializeApp(firebaseConfig);
 

  export default firebase;