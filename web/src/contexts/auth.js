import React, { useState, createContext, useEffect } from 'react';
import firebase from '../services/firebaseConnection';
import { toast } from 'react-toastify';

export const AuthContext = createContext({}); //context criado, 

function AuthProvider({ children }) {
  const [user, setUser] = useState(null); //usuario criado = null(sem usuario)
  const [loadingAuth, setLoadingAuth] = useState(false); //iniciando como false fazendo requisicao de login
  const [loading, setLoading] = useState(true);//comeca carregando como true, sem usuario sera false

  //verificando se existe um usuario logado
  useEffect(() => {
    function loadStorage() {
      const storageUser = localStorage.getItem('SistemaUser'); //localStorage - no firebase

      //se existir um usuario ou algo no storageUser
      if (storageUser) {
        setUser(JSON.parse(storageUser)); //Json.parse = convertendo de volta p um objeto
        setLoading(false);
        toast.success('Usuário já logado!');
      }
      setLoading(false);
    }
    loadStorage();
  }, [])

  //login do usuario
  async function signIn(email, password) {
    setLoadingAuth(true);
    await firebase.auth().signInWithEmailAndPassword(email, password) //passa email e senha
      .then(async (value) => {
        let uid = value.user.uid; //id do usuario criado

        //verificando cadastro
        const userProfile = await firebase.firestore().collection('users')
          .doc(uid)  //cria um documento no BD com os dados fornecido pelo o usuario
          .get()

        let data = {
          uid: uid,
          nome: userProfile.data().nome,
          avatarUrl: userProfile.data().avatarUrl,
          email: value.user.email
        };

        setUser(data);
        storageUser(data);
        setLoadingAuth(false);
        toast.success('Bem vindo de volta!');
      })
      .catch((error) => {
        console.log(error);
        toast.error('Usuário não existe!');
        setLoadingAuth(false);
      })
  }

  //Cadastrando um novo usuario
  async function signUp(email, password, nome) {
    setLoadingAuth(true);

    await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password) //criando no banco
      .then(async (value) => {
        let uid = value.user.uid; //recebendo usuario
        //cadastrando user
        await firebase.firestore().collection('users')
          .doc(uid)
          .set({
            nome: nome,
            avatarUrl: null,
          })
          .then(() => {
            let data = {
              uid: uid,
              nome: nome,
              email: value.user.email,
              avatarUrl: null
            };
            setUser(data); //chamando funcao deslogou
            storageUser(data); //chamando funcao imagem
            setLoadingAuth(false); //chamando funcao autentificado logado
            toast.success('Bem vindo a plataforma!');
          })
      })
          .catch((error) => {
            console.log(error);
            toast.error('Algo deu errado!');
            setLoadingAuth(false);
      })
  }

  //salvando um item (imagem) no storage do firebase - SistemaUser chave
  function storageUser(data) {
    localStorage.setItem('SistemaUser', JSON.stringify(data));
  }

  //Logout do usuario
  async function signOut() {
    await firebase.auth().signOut(); //deslogando 
    localStorage.removeItem('SistemaUser'); //removendo deslogando
    setUser(null);  //volta o estado inicial do usuario
  }

  //AuthContet = 
  return (
    <AuthContext.Provider
      value={{
        signed: !!user,  //!! converte o que tiver no user em booleano
        user,
        loading,
        signUp,
        signOut,
        signIn,
        loadingAuth,
        setUser,
        storageUser
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
export default AuthProvider;
