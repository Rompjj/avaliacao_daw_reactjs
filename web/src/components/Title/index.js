import './title.css';
import React from 'react';

function Title({ children, name }) {
  return (
    <div className="title">
      {children}
      <span>{name}</span>
    </div>
  )
}

export default Title;