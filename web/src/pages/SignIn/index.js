
import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom'; //criar nova conta, atualiza a pagina

import { AuthContext } from '../../contexts/auth';
import './signIn.css';
import logo from '../../assets/logo.png';

function SignIn() {
  const [email, setEmail] = useState(''); // email armazenado no useState
  const [password, setPassword] = useState('');// password armazenado no useState

  const { signIn, loadingAuth } = useContext(AuthContext);

  function handleSubmit(e) {
    e.preventDefault();//nao atualizar a pagina

    if (email !== '' && password !== '') {
      signIn(email, password)
    }
  }

  return (
    <div className="container-center">
      <div className="login">
        <div className="login-area">
          <img src={logo} alt="Sistema Logo" />
        </div>

        <form onSubmit={handleSubmit}>
          <h1>Entrar</h1>
          <input type="text" placeholder="email@email.com" value={email} onChange={(e) => setEmail(e.target.value)} />
          <input type="password" placeholder="*******" value={password} onChange={(e) => setPassword(e.target.value)} />
          <button type="submit">{loadingAuth ? 'Carregando...' : 'Acessar'}</button>
        </form>

        <Link to="/register">Criar uma conta</Link>
      </div>
    </div>
  );
}

export default SignIn;
//login