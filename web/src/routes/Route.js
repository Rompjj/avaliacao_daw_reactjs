import { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../contexts/auth';

//RouteWrapper - recebe a prop do component isprivate e o restante e passado pelo ...rest
function RouteWrapper({
  component: Component,
  isPrivate,
  ...rest
}) {

  const { signed, loading } = useContext(AuthContext);

  if (loading) {
    return (
      <div></div>
    )
  }
  //nao esta autentificado e a rota e privada
  if (!signed && isPrivate) {
    return <Redirect to="/" />
  }
  //esta autentificado e a rota nao e privada
  if (signed && !isPrivate) {
    return <Redirect to="/dashboard" />
  }

  return (
    <Route
      {...rest}
      render={props => (
        <Component {...props} />
      )}
    />
  )
}

export default RouteWrapper;